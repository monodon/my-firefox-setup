# My Firefox Setup

![Alt text](https://files.mastodon.online/media_attachments/files/111/663/234/470/305/935/original/4441f60e16682204.png)


## Disclaimer

My setup is a Frankenstein-style creation, assembled from various sources, including guides and tutorials.

I have enabled vertical tabs using the Sidebery Add-on. It can be found in the browser's extension store.

## Resources:

- [Betterfox](https://github.com/yokoffing/BetterFox)
- [FirefoxCSS store](https://firefoxcss-store.github.io)
- [Firefox CSS hacks by MrOtherGuy](https://github.com/MrOtherGuy/firefox-csshacks)